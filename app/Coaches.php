<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Coaches extends Model
{
    protected $hidden = ['deleted_at','created_at','updated_at'];
    protected $with = ['courses'];
    protected $fillable = ['name','phone','email','identify_number'];
    //protected $appends = ['media1'];

    public function courses()
    {
        return $this->hasMany(Courses::class,'coach_id');
    }

  /*  public function company_subcategory()
    {
        return $this->hasMany(CompanySubcategory::class);
    }*/


   /* public function companies()
    {
        return $this->hasManyThrough(Company::class, CompanySubcategory::class);
    }


    public function getMedia1Attribute()
    {
        $this->loadMedia();
        return $this->media[0]->getUrl();
    }*/

}
