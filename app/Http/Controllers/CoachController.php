<?php

namespace App\Http\Controllers;

use App\Coaches;
use App\CompanyCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CoachController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Coatches = Coaches::all();
        return view('Coatches.index', ['Coatches' => $Coatches]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Coatches.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $Coatch = new Coaches();
        $Coatch->fill($request->all());
        $Coatch->is_nutritionist = "0";
        $Coatch->save();
        return redirect('/Coachs');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $Coatches = Coaches::Find($id);
        $CoatchesArray[0] = $Coatches;

        if(count($Coatches))
            return view('Coatches.index', ['Coatches' => $CoatchesArray]);
        else
            return ("אין תוצאות");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Coatch = Coaches::Find($id);

        if(count($Coatch))
            return view('Coatches.edit', ['Coatch' => $Coatch]);
        else
            return ("אין תוצאות");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Coaches $Coatch)
    {
        $Coatch = Coaches::Find($request->id);
        $Coatch->fill($request->all());
        $Coatch->update();
        return redirect('/Coachs');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Coatch = new Coaches();
        $Coatch = Coaches::Find($id);
        $Coatch->delete();
        return redirect('/Coachs');
    }
}
